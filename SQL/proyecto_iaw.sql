-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: proyecto_iaw
-- ------------------------------------------------------
-- Server version	10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mensajes`
--

DROP TABLE IF EXISTS `mensajes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mensajes` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `remitente` varchar(30) DEFAULT NULL,
  `destinatario` varchar(30) DEFAULT NULL,
  `mensaje` varchar(1000) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `prioridad` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `remitente` (`remitente`),
  KEY `destinatario` (`destinatario`),
  CONSTRAINT `mensajes_ibfk_1` FOREIGN KEY (`remitente`) REFERENCES `usuarios` (`usuario`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mensajes_ibfk_2` FOREIGN KEY (`destinatario`) REFERENCES `usuarios` (`usuario`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `nombre` varchar(20) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `usuario` varchar(30) NOT NULL,
  `clave` varchar(100) DEFAULT NULL,
  `rol` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES ('Admin','Admin','Admin','$2y$10$TjrxnqNqsLJ9YdY6eKZxpOBPjNmYfbca40FeFr.jzezhdNtZ8nc22','administrador'),('Alejandro','Miquel','Alex','$2y$10$VKIQndGKfSEGF6N1QxFMXOIjslAkkYXIpZmoK0txHRWPRhcH/9aIC','administrador'),('Antonio','dsa','antonio','$2y$10$E7caK3UiwBI4SxC1GPzaVe2sA2YyamnpMNKGhBoFOdKxrM/Ah0OPG','usuario'),('david','Santacruz','David','$2y$10$JXIYA4Bva7eY9LBe0jEOh.xnFVAwJ2JGRY9/y37vQFj3GPLlKTavS','usuario'),('Jamel','Ads','jamel','$2y$10$ueWX.dKc3JFxxHFBsM6zAuSoWljakT8YYxa7brVT.CjT8U9lvrch.','usuario'),('Jamelgo','Ads','jamelho','$2y$10$1sjpG900quoR0lfOny2EreWZRlcgMKZMDtn3HlCIHCDWa7H42kHGu','usuario');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;



--
-- Dumping data for table `mensajes`
--

LOCK TABLES `mensajes` WRITE;
/*!40000 ALTER TABLE `mensajes` DISABLE KEYS */;
INSERT INTO `mensajes` VALUES (0000000010,'David','antonio','asdfasdfa','2018-02-16','Alta'),(0000000011,'David','Alex','asdfasdfas','2018-02-16','Baja'),(0000000012,'antonio','Admin','asdfasdfas','2018-02-16','Baja'),(0000000013,'antonio','Admin','asdfasdfsadvsfdbgwertwe','2018-02-16','Media'),(0000000014,'antonio','Admin','adsfasdfsadfsadfa','2018-02-16','Alta');
/*!40000 ALTER TABLE `mensajes` ENABLE KEYS */;
UNLOCK TABLES;


-- Dump completed on 2018-02-16 13:42:07
