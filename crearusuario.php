<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8"/>
  <title>Crear Usuario</title>
  <link rel="stylesheet" type="text/css" href="estilo_form.css">
</head>
<body>
<!-- COMPROBAR ESTADO -->
<?php

require_once "include_mysql.php";
require_once "include_vars.php";
include "Indexadmin.php";

    $numerror=0;


	$valores= array(
		'nombre' =>array("",""),
		'apellido' =>array("",""),
		'usuario' =>array("",""),
		'clave' =>array("",""),
		'rol' =>array("","")
	);


	$sqlBD = SqlConecta($hostSql, $userSql, $passSql, $basedatosSql);


	
	$muestraFormulario=true;
	if (isset($_POST['btnGrabar'])) {
		
		/* NOMBRE */
		if(!isset($_POST['nombre']))
		{
			$valores['nombre'][1]="El campo -NOMBRE- no puede ser vacío";
		} 
		
		else 
		{
			$valores['nombre'][0]=addslashes(trim($_POST['nombre']));
				if ($valores['nombre'][0]=="") 
					{
						$valores['nombre'][1]="El campo -NOMBRE- no puede ser vacío";
					}
		}

		/* APELLIDO */
		if(!isset($_POST['apellido']))
		{
			$valores['apellido'][1]="El campo -APELLIDO- no puede ser vacío";
		} 
		
		else 
		{
			$valores['apellido'][0]=addslashes(htmlentities(trim($_POST['apellido'])));
				
				if ($valores['apellido'][0]=="") 
				{
					$valores['apellido'][1]="El campo -APELLIDO- no puede ser vacío";
				}
		}

		/* USUARIO */
		if(!isset($_POST['usuario']))
		{
			$valores['usuario'][1]="El campo -USUARIO- no puede ser vacío";
		} 
		
		else 
		{
			$valores['usuario'][0]=addslashes(trim($_POST['usuario']));
				if ($valores['usuario'][0]=="") 
				{
					$valores['usuario'][1]="El campo -USUARIO- no puede ser vacío";
				}
		}

		/* CLAVE */
		if(!isset($_POST['clave']))
		{
			$valores['clave'][1]="El campo -CLAVE- no puede ser vacío";
		} 
		
		else 
		{
			$valores['clave'][0]=addslashes(trim($_POST['clave']));
				if ($valores['clave'][0]=="") 
				{
					$valores['clave'][1]="El campo -CLAVE- no puede ser vacío";
				}
		}
		
		
		/* PROCESO */
		if ( ($valores['nombre'][1]=="") && ($valores['apellido'][1]=="") 
			  && ($valores['usuario'][1]=="")
			&& ($valores['clave'][1]=="")
			) 
		{
			$sqlIns="INSERT INTO usuarios ( nombre, apellido, usuario, clave, rol)
					VALUES 
						('".$valores['nombre'][0]."',
						 '".$valores['apellido'][0]."',
						 '".$valores['usuario'][0]."',
						 '".password_hash($valores['clave'][0],PASSWORD_DEFAULT)."',
						 '".$_POST['rol']."'
						 );";
						 
			
			SqlIniTrans($sqlBD);					
			$sqlCursor = SqlQuery($sqlBD, $sqlIns);
			if (!$continuaSql) 
			{
				$numerror=$sqlBD->errno;
				$muestraFormulario=true;
			} 
			
			else 
			{
				$muestraFormulario=true;
			}
			
			SqlFinTrans($sqlBD);
			
		} 
	}
			
			
		/*DELETE */
	if(isset($_GET['delete'])){
		$delete=addslashes(htmlentities(trim($_GET['delete'])));
		
		if ($delete!="") {
			$sqlConsulta = "DELETE FROM usuarios WHERE usuario='".$delete."'";
			SqlIniTrans($sqlBD);					
			$sqlCursor = SqlQuery($sqlBD, $sqlConsulta);
			SqlFinTrans($sqlBD);
		}
	}
			
			
			
	if ($muestraFormulario) {	
?>


<!-- FORMULARIO -->
<div class="formulario">
	<div class="formulario-interno">
	  <form 
		id="idFormulario" 
		name="nFormulario" 
		method="POST" action="#">
		
		<div class="form_description">
			<h2>Crear nuevo usuario</h2>
			<?php
				if (!$continuaSql) {
					if ($numerror==1062) {
						echo "<span style='color:red'>Este usuario ya existe</span><br>";
					} else {
						echo "<span style='color:red'>".$errorSql."</span><br>";			
					}
				}
			?>
		</div>						
		
		<!-- NOMBRE -->
		<div class="caja">
			<div class="caja-label">
				<label for="nombre">NOMBRE</label>
			</div>
			
			<div class="caja-input">
				<input 
					id="nombre" 
					name="nombre"
					
						style="width:95%; max-width:400px"
						type="text" 
						maxlength="100" 
						value="<?php echo $valores['nombre'][0]; ?>"
					/> 
				<?php
					if ($valores['nombre'][1]!="") {
							echo "<br><span style='color:red'>".$valores['nombre'][1]."</span>";
					}
				?>
			</div>
		</div> 		
		
		<!-- APELLIDO -->
		<div class="caja">
			<div class="caja-label">
				<label for="nombre">APELLIDO</label>
			</div>
			
			<div class="caja-input">
				<input 
					id="apellido" 
					name="apellido"
					
						style="width:95%; max-width:400px"
						type="text" 
						maxlength="100" 
						value="<?php echo $valores['apellido'][0]; ?>"
					/> 
				<?php
					if ($valores['apellido'][1]!="") {
							echo "<br><span style='color:red'>".$valores['apellido'][1]."</span>";
					}
				?>
			</div>
		</div> 		

		
		
		<!-- USUARIO -->
		<div class="caja">
			<div class="caja-label">
				<label for="usuario">USUARIO</label>
			</div>
			
			<div class="caja-input">
				<input 
					id="usuario" 
					name="usuario"
					
						style="width:95%; max-width:400px"
						type="text" 
						maxlength="20" 
						value="<?php echo $valores['usuario'][0]; ?>"
					/> 
				<?php
					if ($valores['usuario'][1]!="") {
							echo "<br><span style='color:red'>".$valores['usuario'][1]."</span>";
					}
				?>
			</div>
		</div> 		

		<!-- CLAVE -->
		<div class="caja">
			<div class="caja-label">
				<label for="clave">CLAVE</label>
			</div>
			
			<div class="caja-input">
				<input 
					id="clave" 
					name="clave"
					
						style="width:95%; max-width:400px"
						type="password" 
						maxlength="20" 
						value="<?php echo $valores['clave'][0]; ?>"
					/> 
				<?php
					if ($valores['clave'][1]!="") {
							echo "<br><span style='color:red'>".$valores['clave'][1]."</span>";
					}
				?>
			</div>
		</div>
		
		
		<!-- ROL -->
		<div class="caja">
			<div class="caja-label">
				<label for="clave">ROL</label>
			</div>
				<div class="caja-input">
					<select name="rol" id="rol">
										 <option value="usuario">usuario</option>
										 <option value="administrador">administrador</option>
					</select>
				</div>
		</div>
		
		<!-- BOTÓN GRABAR -->
		<input 
				id="idGrabar"
				name="btnGrabar"
				type="submit"  
				value="Insertar"
		/>

	  </form>	
	</div>
</div>
<?php 	
	} // muestraFormulario

?>		
		<div style="text-align:center">
			<div style="display:inline-block;">
				<p style="font-size:30px">Lista de usuarios</p>
			</div>
		</div>		

<?php		
				$sqlConsulta = "SELECT * FROM usuarios WHERE usuario!='".$_SESSION['usuario']."'";
		$sqlCursor = SqlQuery($sqlBD, $sqlConsulta);
		if (SqlNumRegistros($sqlBD, $sqlCursor)>0) { ?>	
		<table class="tablaForm">
		  <thead>
			<tr>
				<th>Usuario</th>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Rol</th>
				<th style="background-color:white; border: 1px solid white; border-bottom: 1px;;">
			</tr>
		  </thead>		
		  <tbody>
	<?php 		while ($sqlRegistro = SqlObtenerRegistro($sqlBD, $sqlCursor)) { ?>
							<tr>
								<td><?php echo $sqlRegistro['usuario']; ?></td>
								<td><?php echo $sqlRegistro['nombre']; ?></td>
								<td><?php echo $sqlRegistro['apellido']; ?></td>
								<td><?php echo $sqlRegistro['rol']; ?></td>
								<td><a style="text-decoration:none; color:red" href="crearusuario.php?delete=<?php echo $sqlRegistro['usuario']; ?>">Borrar</a></td>
							</tr>
	<?php 		} ?>
		  </tbody>
		</table>
<?php		SqlFree($sqlBD, $sqlCursor);
		}
?>



		
<?php
	SqlDesconecta($sqlBD);

	if (!$continuaSql) {
		echo $errorSql;
	}
	
	
?>

<!-- FIN DE CÓDIGO HTML  -->
</body>
</html>