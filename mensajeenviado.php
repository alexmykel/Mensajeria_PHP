<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8"/>
  <title>Enviar Mensaje</title>
  <link rel="stylesheet" type="text/css" href="estilo_form.css">
</head>
<body>
<!-- COMPROBAR ESTADO -->
<?php

require_once "include_mysql.php";
require_once "include_vars.php"; 	

// MENU
	
	if ($_SESSION['rol']=="administrador")
	{
		 include "Indexadmin.php";
	}
	else
	{
		include "Indexcliente.php";
	}	



    $numerror=0;


	$valores= array(
		'remitente' =>array("",""),
		'destinatario' =>array("",""),
		'mensaje' =>array("",""),
		'prioridad' =>array("","")
	);


	$sqlBD = SqlConecta($hostSql, $userSql, $passSql, $basedatosSql);


	
	$muestraFormulario=true;
	if (isset($_POST['btnGrabar'])) {
		

		/* MENSAJE */
		if(!isset($_POST['mensaje']))
		{
			$valores['mensaje'][1]="El campo -MENSAJE- no puede ser vacío";
		} 
		
		else 
		{
			$valores['mensaje'][0]=addslashes(trim($_POST['mensaje']));
				if ($valores['mensaje'][0]=="") 
				{
					$valores['mensaje'][1]="El campo -MENSAJE- no puede ser vacío";
				}
		}		
		
		/* PROCESO */
		if ( ($valores['destinatario'][1]=="") && ($valores['mensaje'][1]=="") ) 
		{

			$fecha='now()';
			$sqlIns="INSERT INTO mensajes (remitente,destinatario, mensaje, fecha, prioridad)
					VALUES 
						(
						 '".$_SESSION['usuario']."',
						 '".$_POST['destinatario']."',
						 '".$valores['mensaje'][0]."',
						 ".$fecha."".",
						 '".$_POST['prioridad']."'
						 );";
						 

	
			SqlIniTrans($sqlBD);					
			$sqlCursor = SqlQuery($sqlBD, $sqlIns);
			if (!$continuaSql) 
			{
				$numerror=$sqlBD->errno;
				$muestraFormulario=true;
			} 
			
			else 
			{
				$muestraFormulario=true;
			}
			
			SqlFinTrans($sqlBD);
	
		} 
	}
			
	if ($muestraFormulario) {	
?>
<!-- FORMULARIO -->
<div class="formulario">
	<div class="formulario-interno">
	  <form 
		id="idFormulario" 
		name="nFormulario" 
		method="POST" action="#">
		
		<div class="form_description">
			<h2>Enviar Mensaje</h2>
		</div>						
		
		<!-- REMITENTE -->
		<div class="caja">
			<div class="caja-label">
				<label for="nombre">Recibido</label>
			</div>
			
			<div class="caja-label">
				 <?php

						
						echo "".$_SESSION['usuario']."";
					
				?>
					
			</div>
		</div> 		
		
		<!-- DESTINATARIO -->
		<div class="caja">
			<div class="caja-label">
				<label for="nombre">Destinatario</label>
			</div>
			
			<div class="caja-input">
				
			<select name="destinatario">
			
			<?php
				$sqlConsulta = "SELECT * FROM usuarios WHERE usuario !='".$_SESSION['usuario']."'";
							$sqlCursor = SqlQuery($sqlBD, $sqlConsulta);
							if (SqlNumRegistros($sqlBD, $sqlCursor)>0) { 
						while ($sqlRegistro = SqlObtenerRegistro($sqlBD, $sqlCursor)){

					?><option><?php echo $sqlRegistro['usuario'] ?></option>
						
						
				<?php 
						
							
						}
					SqlFree($sqlBD, $sqlCursor);
					
				}
				
				if(!isset($_POST['mensaje']))
		{
			$valores['mensaje'][1]="El campo -MENSAJE- no puede ser vacío";
		} ?>
				
				
				

			</select>		
			</div>
		</div> 		

		
		
		<!-- MENSAJE -->
		<div class="caja">
			<div class="caja-label">
				<label for="usuario">Mensaje</label>
			</div>
			
			<div class="caja-input">
			 
				<textarea name="mensaje" rows="10" cols="40" value="Introduzca su mensaje"> </textarea>
				
			</div>
		</div> 		
		
		
		<!-- PRIORIDAD -->
		<div class="caja">
			<div class="caja-label">
				<label for="usuario">Mensaje</label>
			</div>
			
			<div class="caja-input">
			 
						<select name="prioridad" id="prioridad">
									 <option value="Baja">Baja</option>
									 <option value="Media">Media</option>
									 <option value="Alta">Alta</option>
						</select>
				
			</div>
		</div> 		
		

		
		

		
		
		<!-- BOTÓN GRABAR -->
		<input 
				id="idGrabar"
				name="btnGrabar"
				type="submit"  
				value="Enviar" />

	  </form>	
	</div>
</div>
<?php 	
	} // muestraFormulario

?>		
		<div style="text-align:center">
			<div style="display:inline-block;">
				<p style="font-size:30px">Mensajes enviados</p>
			</div>
		</div>		

<?php		
		$sqlConsulta = "SELECT * FROM mensajes WHERE remitente='".$_SESSION['usuario']."'";
		$sqlCursor = SqlQuery($sqlBD, $sqlConsulta);
		if (SqlNumRegistros($sqlBD, $sqlCursor)>0) { ?>	
		<table class="tablaForm">
		  <thead>
			<tr>
				<th>Fecha</th>
				<th>Destinatario</th>
				<th>Mensaje</th>
				<th>Prioridad</th>
			</tr>
		  </thead>		
		  <tbody>
	<?php 		while ($sqlRegistro = SqlObtenerRegistro($sqlBD, $sqlCursor)) {
				
						
						

	?>-
							<tr>
								<td><?php echo $sqlRegistro['fecha']; ?></td>
								<td><?php echo $sqlRegistro['destinatario']; ?></td>
								<td><?php echo $sqlRegistro['mensaje']; ?></td>
								<td><?php echo $sqlRegistro['prioridad']; ?></td>
							</tr>
	<?php 		} ?>
		  </tbody>
		</table>
<?php		SqlFree($sqlBD, $sqlCursor);
		}
?>



		
<?php
	SqlDesconecta($sqlBD);

	if (!$continuaSql) {
		echo $errorSql;
	}
	
	
?>

<!-- FIN DE CÓDIGO HTML  -->
</body>
</html>