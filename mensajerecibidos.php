<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8"/>
  <title>Mensajes recibidos</title>
  <link rel="stylesheet" type="text/css" href="estilo_form.css">
</head>
<body>
<!-- COMPROBAR ESTADO -->
<?php

require_once "include_mysql.php";
require_once "include_vars.php";
// MENU
	
	if ($_SESSION['rol']=="administrador")
	{
		 include "Indexadmin.php";
	}
	else
	{
		include "Indexcliente.php";
	}	


	$sqlBD = SqlConecta($hostSql, $userSql, $passSql, $basedatosSql);
	
	
?>

<div style="text-align:center">
<div style="display:inline-block;">
	<p style="font-size:30px">Mensajes Recibidos:</p>
</div>
		

<?php		
		$sqlConsulta = "SELECT * FROM mensajes WHERE destinatario='".$_SESSION['usuario']."'";
		$sqlCursor = SqlQuery($sqlBD, $sqlConsulta);
		if (SqlNumRegistros($sqlBD, $sqlCursor)>0) { ?>	
		
		<table class="tablaForm">
		  <thead>
			<tr>
				<th>Fecha</th>
				<th>Remitente</th>
				<th>Mensaje</th>
				<th>Prioridad</th>
			</tr>
		  </thead>		
		  <tbody>
		  
		  
	<?php 		while ($sqlRegistro = SqlObtenerRegistro($sqlBD, $sqlCursor)) { ?>

	
							<tr style="background-color:<?php
									/*Condicion de colores*/
									 if ($sqlRegistro['prioridad'] == 'Alta')
									 {
										$color="red";
									 }
									else if ($sqlRegistro['prioridad'] == 'Media')
									{
										$color="orange";
									}
									else
									{
										$color="green";
									}	
										echo $color;?>" >
								<td><?php echo $sqlRegistro['fecha']; ?></td>
								<td><?php echo $sqlRegistro['remitente']; ?></td>
								<td><?php echo $sqlRegistro['mensaje']; ?></td>
								<td><?php echo $sqlRegistro['prioridad']; ?></td>
							</tr>
	<?php 		} ?>
		  </tbody>
		</table>
<?php		SqlFree($sqlBD, $sqlCursor);
		}
?>



		
<?php
	SqlDesconecta($sqlBD);

	if (!$continuaSql) {
		echo $errorSql;
	}
	
	
?>

<!-- FIN DE CÓDIGO HTML  -->
</body>
</html>