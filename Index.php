<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8"/>
  <title>Inicio de sesion</title>
  <link rel="stylesheet" type="text/css" href="estilo_form.css">
</head>
<body>
<!-- COMPROBAR ESTADO -->
<?php

require_once "include_mysql.php";
require_once "include_vars.php";



$numerror=0;

$estado="FORM"; // FORM, DATOS

	$valores= array(
		'usuario' => array("",""),
		'clave' =>array("","")
	);


	$sqlBD = SqlConecta($hostSql, $userSql, $passSql, $basedatosSql);

	if (isset($_GET['desconectar'])) 
	{
		unset($_SESSION['nombre']);
		unset($_SESSION['apellido']);
		unset($_SESSION['usuario']);
		unset($_SESSION['clave']);
		unset($_SESSION['rol']);
		$estado="FORM";		
	}
	
	
	
	
	if (isset($_SESSION['usuario'])) 
	{
			$sqlConsulta="SELECT * FROM usuarios WHERE
						usuario='".$_SESSION['usuario']."' AND
						clave='".$_SESSION['clave']."'";

			
			
			$sqlCursor = SqlQuery($sqlBD, $sqlConsulta);
			if (SqlNumRegistros($sqlBD, $sqlCursor)>0) { 
				while ($sqlRegistro = SqlObtenerRegistro($sqlBD, $sqlCursor)) { 
				   $_SESSION['nombre']=$sqlRegistro['nombre'];
				   $_SESSION['apellido']=$sqlRegistro['apellido'];
				   $_SESSION['usuario']=$sqlRegistro['usuario'];
				   $_SESSION['clave']=$sqlRegistro['clave'];
				   $_SESSION['rol']=$sqlRegistro['rol'];
				   $estado="DATOS";
				   
				}
				SqlFree($sqlBD, $sqlCursor);
			} else {
			   echo "<strong>SESSION</strong>: Usuario NO existe<br><br>";
			   unset($_SESSION['nombre']);
			   unset($_SESSION['apellido']);
			   unset($_SESSION['usuario']);
			   unset($_SESSION['clave']);
			   unset($_SESSION['rol']);
			   
			   $estado="FORM";
			}
		
	
	}
	
	if (isset($_POST['btnGrabar'])) 
	{
		
		/* USUARIO */
		
		if(!isset($_POST['usuario']))
		{
			$valores['usuario'][1]="El campo -USUARIO- no puede ser vacío";
		} 
		
		else 
		{
			$valores['usuario'][0]=addslashes(htmlentities(trim($_POST['usuario'])));
			if ($valores['usuario'][0]=="") 
			{
				$valores['usuario'][1]="El campo -USUARIO- no puede ser vacío";
			}
		}

		/* CLAVE */
		
		if(!isset($_POST['clave']))
		{
			$valores['clave'][1]="El campo -CLAVE- no puede ser vacío";
		} 
		
		else 
		{
			$valores['clave'][0]=addslashes(htmlentities(trim($_POST['clave'])));
			if ($valores['clave'][0]=="") 
			{
				$valores['clave'][1]="El campo -CLAVE- no puede ser vacío";
			}
		}
		


		/* PROCESO */
		if ( ($valores['usuario'][1]=="") && ($valores['clave'][1]=="") ) {
			$sqlConsulta="SELECT * FROM usuarios WHERE
						usuario='".$valores['usuario'][0]."'";

			
			
			$sqlCursor = SqlQuery($sqlBD, $sqlConsulta);
			if (SqlNumRegistros($sqlBD, $sqlCursor)>0) { 
				while ($sqlRegistro = SqlObtenerRegistro($sqlBD, $sqlCursor)) { 
				    if (password_verify($valores['clave'][0],$sqlRegistro['clave'])) {
					   $_SESSION['nombre']=$sqlRegistro['nombre'];
					   $_SESSION['apellido']=$sqlRegistro['apellido'];
					   $_SESSION['usuario']=$sqlRegistro['usuario'];
					   $_SESSION['clave']=$sqlRegistro['clave'];
					   $_SESSION['rol']=$sqlRegistro['rol'];					   
					   $estado="DATOS";
					} else {
					   echo "<strong>POST</strong>: Usuario NO existe<br><br>";
					   unset($_SESSION['nombre']);
					   unset($_SESSION['apellido']);
					   unset($_SESSION['usuario']);
					   unset($_SESSION['clave']);
					   unset($_SESSION['rol']);
					   $estado="FORM";	

					   $valores['usuario'][1]="El campo -USUARIO o CLAVE- no es correcta";
					}
				}
				SqlFree($sqlBD, $sqlCursor);
			} else {
			   echo "<strong>POST</strong>: Usuario NO existe<br><br>";
			   unset($_SESSION['nombre']);
			   unset($_SESSION['apellido']);
			   unset($_SESSION['usuario']);
			   unset($_SESSION['clave']);
			   unset($_SESSION['rol']);
			   $estado="FORM";

			   $valores['usuario'][1]="El campo -USUARIO o CLAVE- no es correcta";
			   
		   }
			
		} 
	}
			
	if ($estado=="FORM") {	
		include "cabecera.php";
?>
<!-- FORMULARIO -->
<div class="formulario">
	<div class="formulario-interno">
	  <form 
		id="idFormulario" 
		name="nFormulario" 
		method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		
		<div class="form_description">
			<h2>Introduzca su usuario</h2>
		</div>						

		<!-- USUARIO -->
		<div class="caja">
			<div class="caja-label">
				<label for="usuario">Usuario</label>
			</div>
			
			<div class="caja-input">
				<input 
					id="usuario" 
					name="usuario"
					
						style="width:95%; max-width:80px"
						type="text" 
						maxlength="20" 
						value="<?php echo $valores['usuario'][0]; ?>"
					/> 
				<?php
					if ($valores['usuario'][1]!="") {
							echo "<br><span style='color:red'>".$valores['usuario'][1]."</span>";
					}
				?>
			</div>
		</div> 
		
		<!-- CLAVE -->
		<div class="caja">
			<div class="caja-label">
				<label for="clave">Contraseña</label>
			</div>
			
			<div class="caja-input">
				<input 
					id="clave" 
					name="clave"
					
						style="width:95%; max-width:400px"
						type="password" 
						maxlength="20" 
						value="<?php echo $valores['clave'][0]; ?>"
					/> 
				<?php
					if ($valores['clave'][1]!="") {
							echo "<br><span style='color:red'>".$valores['clave'][1]."</span>";
					}
				?>
			</div>
		</div> 		

		
		<!-- BOTÓN GRABAR -->
		<input 
				id="idGrabar"
				name="btnGrabar"
				type="submit"  
				value="Entrar" />

	  </form>	
	</div>
</div>
<?php 	
	} // FORM
	
	if (($estado=="DATOS") && ($_SESSION['rol']=="administrador")) {
		 include "Indexadmin.php";
?>

		<div style="text-align:center">
			<div style="display:inline-block;">
				<p style="font-size:30px">Usted está identificado como:</p>
			</div>
		</div>

		<table class="tablaForm">
			<colgroup>
				<col style="background-color:orange" />
				<col />
			</colgroup>
			<tr>
				<td>Usuario</td>
				<td><?php echo $_SESSION['usuario']; ?></td>
			</tr>
			<tr>
				<td>Nombre</td>
				<td><?php echo $_SESSION['usuario']; ?></td>
			</tr>
			<tr>
				<td>Apellido</td>
				<td><?php echo $_SESSION['nombre']; ?></td>
			</tr>

		</table>
		
		
		<div style="text-align:center">
			<div style="display:inline-block;">
				<a class="botonMenu" href="<?php echo $_SERVER['PHP_SELF']; ?>?desconectar=1">Desconectar</a> 
			</div>
		</div>
		
		

<?php		
	} // DATOS
		
	 if (($estado=="DATOS") && ($_SESSION['rol']!="administrador")) {
		 include "Indexcliente.php";
?>

		<div style="text-align:center">
			<div style="display:inline-block;">
				<p style="font-size:30px">Usted está identificado como:</p>
			</div>
		</div>

		<table class="tablaForm">
			<colgroup>
				<col style="background-color:orange" />
				<col />
			</colgroup>
			<tr>
				<td>Usuario</td>
				<td><?php echo $_SESSION['usuario']; ?></td>
			</tr>
			<tr>
				<td>Nombre</td>
				<td><?php echo $_SESSION['usuario']; ?></td>
			</tr>
			<tr>
				<td>Apellido</td>
				<td><?php echo $_SESSION['nombre']; ?></td>
			</tr>
		</table>
		
		
		<div style="text-align:center">
			<div style="display:inline-block;">
				<a class="botonMenu" href="<?php echo $_SERVER['PHP_SELF']; ?>?desconectar=1">Desconectar</a> 
			</div>
		</div>
		
		

<?php		
	} // DATOS	
	
	
	
	
	SqlDesconecta($sqlBD);

	
	if (!$continuaSql) {
		echo $errorSql;
	}	
	
?>

<!-- FIN DE CÓDIGO HTML  -->
</body>
</html>